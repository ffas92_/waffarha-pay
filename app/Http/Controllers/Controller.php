<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validate(Request $request, array $rules, $status = Response::HTTP_BAD_REQUEST, $message = NULL)
    {
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            response()->json(['message' => ($message) ? $message : $validator->errors()->all(), 'status' => $status], $status)->send();
            return exit;
        }
    }

    public function output($data, $paginate = null, $status = Response::HTTP_OK, $statusOnly = false,
                           $name = "data", $message = null, $extra_data_name = null, $extra_data = null,
                           $extra_data_name2 = NULL, $extra_data2 = NULL, $extra_data_name3 = NULL, $extra_data3 = NULL)
    {
        if ($statusOnly)
            return response()->json(['status' => $status], $status);

        $trueStatus = Response::HTTP_NO_CONTENT;
//        if (!isset($data) || empty((array)$data) || !count((array)$data))
//            return response()->json(['status' => $trueStatus]);
        $response_array = ["$name" => $data, "status" => $status];
        if (isset($extra_data))
            $response_array["$extra_data_name"] = $extra_data;
        if (isset($extra_data2))
            $response_array["$extra_data_name2"] = $extra_data2;
        if (isset($extra_data3))
            $response_array["$extra_data_name3"] = $extra_data3;
        if (isset($paginate))
            $response_array['moreData'] = $paginate;
        if (isset($message))
            $response_array['message'] = $message;

        return response()->json($response_array, $status);
    }


}
