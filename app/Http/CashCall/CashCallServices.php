<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 4:42 PM
 */

namespace App\Http\CashCall;


use App\Http\CashCall\Model\Package;
use App\Http\CashCall\Model\Provider;
use App\Http\CashCall\Model\ProviderService;
use App\Http\CashCall\Model\Service;
use App\Http\CashCall\Model\UserRequest;
use App\Http\CashCall\Service\Topup as TopupService;
use App\Http\CashCall\Service\EVouchers as EVoucherService;
use App\Http\CashCall\Service\BillService as BillService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CashCallServices extends Controller
{
    /** @var  TopupService $topupService */
    protected $topupService;

    /** @var  EVoucherService $eVoucherService */
    protected $eVoucherService;

    /** @var  BillService $billService */
    protected $billService;

    /**
     * CashCallServices constructor.
     * @param TopupService $topupService
     * @param EVoucherService $eVoucherService
     * @param BillService $billService
     */
    function __construct(TopupService $topupService, EVoucherService $eVoucherService, BillService $billService)
    {
        $this->topupService = $topupService;
        $this->eVoucherService = $eVoucherService;
        $this->billService = $billService;
    }

    /**
     * @param Request $request
     * @return ProviderService $provider_service
     */
    public function requestValidation(Request $request)
    {
        $this->validate($request, [
            'provider_id' => 'required|exists:providers,id',
            'service_id' => 'required|exists:services,id',
            'phone' => array("required", "digits:11", "regex:/^[010,011,012,015]{3}\d{8}$/")
        ]);
        $provider_service = ProviderService::with(['provider', 'service'])
            ->where('provider_id', $request->provider_id)
            ->where('service_id', $request->service_id)
            ->first();

        return $provider_service;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function getProviders(Request $request)
    {
        $providers = Provider::where('status', 'active')->get();
        return $this->output($providers, null, Response::HTTP_OK, false, "providers");
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function getServices(Request $request)
    {
        $this->validate($request, [
            'provider_id' => 'required|exists:providers,id'
        ]);
        $prov = Provider::with([
            'service' => function ($query) use ($request) {
                $query->where('provider_id', $request->provider_id)
                    ->where('status', 'active');
            }])
            ->where('id', $request->provider_id)
            ->first();

        return $this->output($prov->service, null, Response::HTTP_OK, false, "services");
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function getPackages(Request $request)
    {
        $this->validate($request, [
            'provider_id' => 'required|exists:providers,id',
            'service_id' => 'required|exists:services,id'
        ]);
        $package = Package::where('provider_id', $request->provider_id)
            ->where('service_id', $request->service_id)
            ->where('status', 'active')
            ->get();
//        dd($package);

        return $this->output($package, null, Response::HTTP_OK, false, "packages");
    }

//    function GeneralService()
//    {
//
//    }

    /**
     * @param Request $request
     * @return mixed
     */
    function Topup(Request $request)
    {
        $provider_service = $this->requestValidation($request);
        $this->validate($request, [
            'amount' => 'required|min:1|integer'
        ]);

        $id = UserRequest::create([
            'provider_code' => $provider_service->provider->code,
            'provider_service_code' => $provider_service->provider->code . '.' . $provider_service->service->code,
            'amount' => $request->amount,
            'phone' => $request->phone,
            'service_type' => $provider_service->type
        ])->id;
//        $id = 9;
        $response = $this->topupService->call($id);

        if ($response['status_code'] == 0) {
            UserRequest::find($id)->update(['txn_id' => $response['gle_txn_id']]);
        }

        return $this->output($response['sys_to_sys_msg']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function TopupPackage(Request $request)
    {
        $provider_service = $this->requestValidation($request);
        $this->validate($request, [
            'package_id' => 'required|exists:packages,id'
        ]);
        $package = Package::find($request->package_id);

        $id = UserRequest::create([
            'provider_code' => $provider_service->provider->code,
            'provider_service_code' => $provider_service->provider->code . '.' . $provider_service->service->code,
            'amount' => $package->amount,
            'phone' => $request->phone,
            'service_type' => $provider_service->type,
            'package_id' => $request->package_id
        ])->id;
        $response = $this->topupService->package($id);
        if ($response['status_code'] == 0) {
            UserRequest::find($id)->update(['txn_id' => $response['gle_txn_id']]);
        }
        return $this->output($response['sys_to_sys_msg']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function EVoucher(Request $request)
    {
        $provider_service = $this->requestValidation($request);
        $this->validate($request, [
            'package_id' => 'required|exists:packages,id'
        ]);
        $package = Package::find($request->package_id);

        $id = UserRequest::create([
            'provider_code' => $provider_service->provider->code,
            'provider_service_code' => $provider_service->provider->code . '.' . $provider_service->service->code,
            'amount' => $package->amount,
            'phone' => $request->phone,
            'service_type' => $provider_service->type,
            'package_id' => $request->package_id
        ])->id;
        $response = $this->eVoucherService->call($id);
        if ($response['status_code'] == 0) {
            UserRequest::find($id)->update(['txn_id' => $response['gle_txn_id']]);
        }
        return $this->output($response['sys_to_sys_msg']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function BillEnquiry(Request $request)
    {
        $provider_service = $this->requestValidation($request);
        $id = UserRequest::create([
            'provider_code' => $provider_service->provider->code,
            'provider_service_code' => $provider_service->provider->code . '.' . $provider_service->service->code,
            'phone' => $request->phone,
            'service_type' => $provider_service->type
        ])->id;

        $response = $this->billService->enquiry($id);

        if ($response['status_code'] == 0) {
            UserRequest::find($id)->update(['txn_id' => $response['gle_txn_id']]);
        }

//        return $this->output($response['sys_to_sys_msg']);
        return $this->output($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function BillPayment(Request $request)
    {
        $provider_service = $this->requestValidation($request);
        $id = UserRequest::create([
            'provider_code' => $provider_service->provider->code,
            'provider_service_code' => $provider_service->provider->code . '.' . 'BE',
            'phone' => $request->phone,
            'service_type' => 'Bill Enquiry'
        ])->id;

        $response = $this->billService->enquiry($id);
        if ($response['status_code'] == 0) {
            UserRequest::find($id)->update(['txn_id' => $response['gle_txn_id']]);

            //pill payment after enquiry
            $p_id = UserRequest::create([
                'provider_code' => $provider_service->provider->code,
                'provider_service_code' => $provider_service->provider->code . '.' . $provider_service->service->code,
                'phone' => $request->phone,
                'service_type' => $provider_service->type,
                'amount' => $response['out_parameters']['OUT_PARAMETER_1']
            ])->id;
//dd($p_id);
            $p_response = $this->billService->payment($p_id, $response['out_parameters']);
//            return ($p_response);
            if ($p_response['status_code'] == 0) {
                UserRequest::find($p_id)->update(['txn_id' => $p_response['gle_txn_id']]);
            }

            return $this->output($p_response);

        }

        return $this->output($response['sys_to_sys_msg']);
    }


}