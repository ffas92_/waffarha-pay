<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 19/11/19
 * Time: 9:43 PM
 */

namespace App\Http\CashCall\Service;

use App\Http\CashCall\Client\PostClient;
use App\Http\CashCall\Constant\Links;
use App\Http\CashCall\Model\UserRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class Auth
{
    /** @var  PostClient $postClient */
    protected $postClient;

    /**
     * Auth constructor.
     * @param PostClient $postClient
     */
    public function __construct(PostClient $postClient)
    {
        $this->postClient = $postClient;
    }

    /**
     * @param UserRequest $row
     * @return string
     */
    public function getHashCode(UserRequest $row): string
    {
        return $this->auth($row);
    }

    /**
     * @return string
     */
    public function getLoginId(): string
    {
        return config('cashcall.login_id');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return config('cashcall.password');
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return config('cashcall.secret_key');
    }

    /**
     * @param string|null $txn_id
     * @param string|null $new_password
     * @return string
     */
    public function getCustomHash(string $txn_id = null, string $new_password = null)
    {
        return $this->customHash($txn_id, $new_password);
    }

    /**
     * @param UserRequest $row
     * @return string
     */
    private function auth(UserRequest $row): string
    {
        return md5($row->provider_code . $row->provider_service_code . "$row->amount" . $row->phone . $this->getLoginId() . $this->getPassword() . $this->getSecretKey());
    }

    /**
     * @param string|null $txn_id
     * @param string|null $new_password
     * @return string
     */
    private function customHash(string $txn_id = null, string $new_password = null): string
    {
        if ($txn_id) {
            return md5($txn_id . $this->getLoginId() . $this->getPassword() . $this->getSecretKey());
        } elseif ($new_password) {
            return md5(Carbon::now()->toDateString() . $new_password . $this->getLoginId() . $this->getPassword() . $this->getSecretKey());
        } else {
//            dd(Carbon::now()->toDateString() . $this->getLoginId() . $this->getPassword() . $this->getSecretKey());
            return md5(Carbon::now()->toDateString() . $this->getLoginId() . $this->getPassword() . $this->getSecretKey());
        }
    }
}