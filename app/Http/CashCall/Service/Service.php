<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 19/11/19
 * Time: 10:32 PM
 */

namespace App\Http\CashCall\Service;

use App\Http\CashCall\Client\PostClient;

class Service
{
    /** @var  PostClient $postClient */
    protected $postClient;

    /** @var  Auth $auth */
    protected $auth;

    /**
     * Service constructor.
     * @param PostClient $postClient
     * @param Auth $auth
     */
    public function __construct(PostClient $postClient, Auth $auth)
    {
        $this->postClient = $postClient;
        $this->auth = $auth;
    }


}