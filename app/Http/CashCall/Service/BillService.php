<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 11:58 AM
 */

namespace App\Http\CashCall\Service;

use App\Http\CashCall\Model\Package;
use App\Http\CashCall\Model\UserRequest;
use App\Http\CashCall\Constant\Links;
use App\Http\CashCall\Model\UserRequestLog;
use Carbon\Carbon;

class BillService extends Service
{
    /**
     * @param int $id
     * @return array
     */
    public function enquiry(int $id)
    {
        $row = UserRequest::find($id);

        $data = [
            "channel_code" => "STS",
            "channel_key" => "STS",
            "terminal_id" => "",
            "client_version" => "V1",
            "login_id" => $this->auth->getLoginId(),
            "password" => $this->auth->getPassword(),
            "hash_code" => $this->auth->getHashCode($row),
            "payment_network_code" => "CACPN",
            "service_provider_code" => $row->provider_code,
            "service_code" => $row->provider_service_code,
            "transaction_value" => "0",
            "user_b_account_id" => $row->phone,
            "external_txn_id" => "$row->id",
            "external_ref_number" => "$row->id",
            "external_txn_date_time" => Carbon::parse($row->created_at)->format('F d, Y h:i:s A'),
//            "external_txn_date_time" => Carbon::now()->format('F d, Y h:i:s A'),
            "in_parameters" => new \stdClass()
        ];
//        dd($data);
        $response = $this->postClient->send(Links::SERVICES_URL, $data);
        UserRequestLog::create(
            [
                'ref_id' => $row->id,
                'req' => json_encode($data),
                'type' => $row->service_type,
                'res' => json_encode($response)
            ]
        );

        return $response;
    }

    /**
     * @param int $id
     * @return array
     */
    public function payment(int $id, $out_parameters)
    {
        $row = UserRequest::find($id);

        $data = [
            "channel_code" => "STS",
            "channel_key" => "STS",
            "terminal_id" => "",
            "client_version" => "V1",
            "login_id" => $this->auth->getLoginId(),
            "password" => $this->auth->getPassword(),
            "hash_code" => $this->auth->getHashCode($row),
            "payment_network_code" => "CACPN",
            "service_provider_code" => $row->provider_code,
            "service_code" => $row->provider_service_code,
            "transaction_value" => "$row->amount",
            "user_b_account_id" => $row->phone,
            "external_txn_id" => "$row->id",
            "external_ref_number" => "$row->id",
            "external_txn_date_time" => Carbon::parse($row->created_at)->format('F d, Y h:i:s A'),
            "in_parameters" => (object) $out_parameters

        ];
//        return $data;
        $response = $this->postClient->send(Links::SERVICES_URL, $data);
        UserRequestLog::create(
            [
                'ref_id' => $row->id,
                'req' => json_encode($data),
                'type' => $row->service_type,
                'res' => json_encode($response)
            ]
        );

        return $response;
    }
}