<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/19
 * Time: 3:10 PM
 */

namespace App\Http\CashCall\Service;


use App\Http\CashCall\Constant\Links;
use App\Http\CashCall\Model\UserRequest;

class InternalApi extends Service
{
    /**
     * @param string $id
     * @return array
     */
    function transactionStatus(string $id)
    {
        $data = [
            "channel_code" => "STS",
            "channel_key" => "STS",
            "terminal_id" => "",
            "client_version" => "V1",
            "login_id" => $this->auth->getLoginId(),
            "password" => $this->auth->getPassword(),
            "hash_code" => $this->auth->getCustomHash($id),
            "payment_network_code" => "CACPN",
            "report_parameters" => ([
                "TRANSACTION_ID" => $id//$id
            ])
        ];


        $response = $this->postClient->send(Links::CUSTOMER_TRANSACTION_URL, $data);

        return $response;
    }

    /**
     * @return array
     */
    function checkBalance()
    {
        $data = [
            "channel_code" => "STS",
            "channel_key" => "STS",
            "terminal_id" => "",
            "client_version" => "V1",
            "login_id" => $this->auth->getLoginId(),
            "password" => $this->auth->getPassword(),
            "hash_code" => $this->auth->getCustomHash(),
            "payment_network_code" => "CACPN",
        ];


        $response = $this->postClient->send(Links::CHECK_BALANCE_URL, $data);

        return $response;
    }

    /**
     * @param string $pass
     * @return array
     */
    function changePassword(string $pass)
    {
        $data = [
            "channel_code" => "STS",
            "channel_key" => "STS",
            "terminal_id" => "",
            "client_version" => "V1",
            "login_id" => $this->auth->getLoginId(),
            "password" => $this->auth->getPassword(),
            "hash_code" => $this->auth->getCustomHash(null, $pass),
            "payment_network_code" => "CACPN",
            "password_type" => 1,
            "new_password" => $pass
        ];


        $response = $this->postClient->send(Links::CHANGE_PASSWORD_URL, $data);

        return $response;

    }
}