<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 19/11/19
 * Time: 10:24 PM
 */

namespace App\Http\CashCall\Client;

class PostClient extends Client
{

    /**
     * @param string $url
     * @param array $body
     * @return array
     */
    public function send(string $url, array $body): array
    {
        return json_decode(
            $this->guzzleClient->post(
                $url, ['json' => $body]
            )->getBody()->getContents(),
            true
        );
    }
}