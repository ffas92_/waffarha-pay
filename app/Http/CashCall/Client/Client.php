<?php

namespace App\Http\CashCall\Client;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 19/11/19
 * Time: 10:22 PM
 */
abstract class Client
{
    /** @var  GuzzleClient $guzzleClient */
    protected $guzzleClient;

    /**
     * Client constructor.
     * @param GuzzleClient $guzzleClient
     */
    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param string $url
     * @param array $body
     * @return array
     */
    abstract public function send(string $url, array $body) : array ;
}