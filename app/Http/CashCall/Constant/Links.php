<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 19/11/19
 * Time: 9:55 PM
 */

namespace App\Http\CashCall\Constant;


final class Links
{
    private const BASE_PORT = "9181";

    private const BASE_URL = 'https://mfstest1.cashcall.com.eg';

    public const SERVICES_URL = self::BASE_URL . ':' . self::BASE_PORT . '/mfsexternaltransactions/STSServiceTXN';
    public const CUSTOMER_TRANSACTION_URL = self::BASE_URL . ':' . self::BASE_PORT . '/mfsexternaltransactions/STSGetCustomerTXN';
    public const CHECK_BALANCE_URL = self::BASE_URL . ':' . self::BASE_PORT . '/mfsexternaltransactions/STSBalanceEnquiry';
    public const CHANGE_PASSWORD_URL = self::BASE_URL . ':' . self::BASE_PORT . '/mfsexternaltransactions/STSChangePassword';
}