<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class UserRequestLog extends Model
{
    protected $table = "user_requests_logs";
    protected $fillable = ['ref_id', 'req', 'res', 'type'];
    public $timestamps = false;
}