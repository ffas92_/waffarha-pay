<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    protected $table = "user_requests";
    protected $fillable = ['provider_code', 'provider_service_code', 'amount', 'phone', 'service_type', 'package_id', 'txn_id'];
    public $timestamps = false;
}