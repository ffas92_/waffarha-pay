<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = "providers";
    protected $hidden = ['created_at', 'status'];

    public function service()
    {
        return $this->hasManyThrough(Service::class, ProviderService::class, 'provider_id', 'id');
    }
}