<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class ProviderService extends Model
{
    protected $table = "providers_services";
    protected $primaryKey = 'service_id';

    public function provider()
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

}