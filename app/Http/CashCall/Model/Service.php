<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "services";
    protected $hidden = ['created_at', 'laravel_through_key'];
}