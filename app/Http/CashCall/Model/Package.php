<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/09
 * Time: 2:01 PM
 */

namespace App\Http\CashCall\Model;


use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = "packages";
    protected $hidden = ['created_at', 'status', 'provider_id', 'service_id'];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }

}