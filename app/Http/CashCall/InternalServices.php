<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/19
 * Time: 2:27 PM
 */

namespace App\Http\CashCall;

use App\Http\Controllers\Controller;
use App\Http\CashCall\Service\InternalApi as InternalApi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InternalServices extends Controller
{
    /**
     * @var InternalApi $internalApi
     */
    protected $internalApi;

    /**
     * InternalServices constructor.
     * @param InternalApi $internalApi
     */
    function __construct(InternalApi $internalApi)
    {
        $this->internalApi = $internalApi;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function getTransactionStatus(Request $request){
//        $request->transaction_id = 'VE.RC200913145018066';
        $this->validate($request, [
            'transaction_id' => 'required|exists:user_requests,txn_id'
        ]);

        $response = $this->internalApi->transactionStatus($request->transaction_id);

        return $this->output($response['status_msg']);
    }

    /**
     * @return mixed
     */
    function checkAccountBalance(){
        $response = $this->internalApi->checkBalance();
        
        if($response['status_code'] == 0){
            return $this->output(round($response['balance_result'][0]['current_balance'], 2), null, Response::HTTP_OK, false, 'current_balance', $response['status_msg']);
        }else{
            return $this->output("Something went wrong", null, Response::HTTP_BADREQUEST, false, 'message');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function changeAccountPassword(Request $request){
        $this->validate($request, [
            'password' => 'required|min:6|confirmed'
        ]);

        $response = $this->internalApi->changePassword($request->password);

        if($response['status_code'] == 0)
            return $this->output($response['status_msg'], null, Response::HTTP_OK, false, 'message');
        else
            return $this->output($response['status_msg'], null, Response::HTTP_OK, false, 'message');
    }

}