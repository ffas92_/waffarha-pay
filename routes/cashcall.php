<?php
/**
 * Created by PhpStorm.
 * User: karawiaaa
 * Date: 20/09/10
 * Time: 11:50 AM
 */

use Illuminate\Support\Facades\Route;

Route::post('get-providers', 'CashCallServices@getProviders');
Route::post('get-services', 'CashCallServices@getServices');
Route::post('get-packages', 'CashCallServices@getPackages');

Route::post('top-up', 'CashCallServices@Topup');
Route::post('top-up-package', 'CashCallServices@TopupPackage');
Route::post('e-voucher', 'CashCallServices@EVoucher');

Route::post('bill-enquiry', 'CashCallServices@BillEnquiry');
Route::post('bill-payment', 'CashCallServices@BillPayment');

Route::post('check-transaction', 'InternalServices@getTransactionStatus');
Route::post('check-balance', 'InternalServices@checkAccountBalance');
Route::post('change-password', 'InternalServices@changeAccountPassword');